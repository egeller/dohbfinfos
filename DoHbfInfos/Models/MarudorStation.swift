//
//  MarudorStation.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 06.01.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation

public class MarudorStation:Codable, Identifiable{
    var coordinates: MarudorStationCoordinates;
    var title: String
     

     
     enum CodingKeys: String, CodingKey {
         case coordinates = "coordinates"
        case title = "title"
         
             
            }
    public init(coords:MarudorStationCoordinates, title:String){
         self.coordinates = coords;
        self.title = title
        
         
     }
     required public init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         self.coordinates = try container.decodeIfPresent(MarudorStationCoordinates.self, forKey: .coordinates) ??
         MarudorStationCoordinates(lat: 0, lng: 0);
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? "";
        
         
         
     }
}
