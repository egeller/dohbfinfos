//
//  MarudorDataContainer.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 01.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import SwiftUI

public class MarudorDataContainer: Codable, Identifiable{
    public var departures: MarudorLines
    
    enum CodingKeys: String, CodingKey {
               case departures = "departures"
            
           }
    public init(departures:MarudorLines){
        self.departures = departures
    }
    required public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.departures = try container.decodeIfPresent(MarudorLines.self, forKey: .departures) ??
        []
    }
    
    
}
