//
//  MarudorLine.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 01.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

public class MarudorLine:Codable, Identifiable{
    
    public let initialDeparture:Int
    public let scheduledDestination:String
    public let routes:MarudorRoutes
    public let destination:String
    public let train:MarudorTrain?
    public let platform:String
    public let messages:MarudorMessage
    public let departure: MarudorDeparture
    public let arrival: MarudorArrival
    
    enum CodingKeys: String, CodingKey {
               case initialDeparture = "initialDeparture"
               case scheduledDestination = "scheduledDestination"
               case destination = "destination"
               case routes = "route"
               case train = "train"
               case platform = "platform"
               case messages = "messages"
               case departure = "departure"
               case arrival = "arrival"
            
           }
    public init(initialDeparture:String, scheduledDestination:String, routes:MarudorRoutes, train:MarudorTrain, destination:String, platform:String, messages: MarudorMessage, departure:MarudorDeparture, arrival:MarudorArrival){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        let datetime = formatter.date(from: initialDeparture)

       

        self.initialDeparture = Int(datetime?.timeIntervalSince1970 ?? 0)
        self.scheduledDestination = scheduledDestination
        self.routes = routes
        self.train = train
        self.platform = platform
        self.messages = messages
        self.arrival = arrival
        self.departure = departure
        self.destination = destination
        
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let initialDepartureAsString = try container.decodeIfPresent(String.self, forKey: .initialDeparture) ??
        ""
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        let datetime = formatter.date(from: initialDepartureAsString)

       

        self.initialDeparture = Int(datetime?.timeIntervalSince1970 ?? 0)
        self.scheduledDestination = try container.decodeIfPresent(String.self, forKey: .scheduledDestination) ??
            "unknown"
        self.destination = try container.decodeIfPresent(String.self, forKey: .destination) ??
        "unknown"
        self.platform = try container.decodeIfPresent(String.self, forKey: .platform) ??
        "unknown"
        self.routes = try container.decodeIfPresent(MarudorRoutes.self, forKey: .routes) ??
        []
        
        self.messages = try container.decodeIfPresent(MarudorMessage.self, forKey: .messages) ??
            MarudorMessage(delays: [], qoses: [])
        self.train = try container.decodeIfPresent(MarudorTrain.self, forKey: .train) ??
        MarudorTrain(name: "unknwon", trainCategory: "unknwon", number: "unknown")
        self.arrival = try container.decodeIfPresent(MarudorArrival.self, forKey: .arrival) ?? MarudorArrival(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "unknown", scheduledPlatform: "unknown", hidden: false, cancelled: false, delay: 0)
        
        self.departure = try container.decodeIfPresent(MarudorDeparture.self, forKey: .departure) ?? MarudorDeparture(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "unknown", scheduledPlatform: "unknown", hidden: false, cancelled: false, delay: 0)
        
        
    }
    
    
}

public typealias MarudorLines = [MarudorLine]
