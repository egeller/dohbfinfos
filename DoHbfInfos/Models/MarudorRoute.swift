//
//  MarudorRoute.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 01.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

public class MarudorRoute:Codable, Identifiable{
    
    public let name:String
    
    enum CodingKeys: String, CodingKey {
               case name = "name"
            
           }
    public init(name:String){
        self.name = name
    }
    required public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.name = try container.decodeIfPresent(String.self, forKey: .name) ??
        "unknown"
    }
    
}

public typealias MarudorRoutes = [MarudorRoute]
