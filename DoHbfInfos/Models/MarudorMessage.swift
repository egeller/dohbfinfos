//
//  MarudorMessage.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 05.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

public class MarudorMessage:Codable, Identifiable{
    public var delays:MarudorDelays
    public var qoses:MarudorQoses
    
    
    enum CodingKeys: String, CodingKey {
        case delays = "delay"
        case qoses = "qos"
      
     
    }
    
    public init(delays:MarudorDelays, qoses:MarudorQoses) {
        self.delays = delays
    
        self.qoses = qoses
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.delays  = try container.decodeIfPresent(MarudorDelays.self, forKey: .delays) ?? []
        self.qoses  = try container.decodeIfPresent(MarudorQoses.self, forKey: .qoses) ?? []
       
        
    }
    
    
}

public typealias MarudorMessages = [MarudorMessage]

