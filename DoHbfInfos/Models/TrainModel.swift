//
//  TrainModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 28.09.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import SwiftUI
/**
 {
   "name": "ICE 758",
   "type": "ICE",
   "boardId": 8000080,
   "stopId": 8000080,
   "stopName": "Dortmund Hbf",
   "dateTime": "2019-09-28T22:28",
   "origin": "München Hbf",
   "track": "18",
   "detailsId": "85692%2F41685%2F168066%2F55469%2F80%3fstation_evaId%3D8000080"
 }
 */


public class Train : Codable, Identifiable {
    public let name: String
    public let type: String
    public let boardId: Int
    public let stopId: Int
    public let dateTime: String
    public let origin: String
    public let track: String
    public let detailsId: String
    
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case type = "type"
        case boardId = "boardId"
        case stopId = "stopId"
        case dateTime = "dateTime"
        case origin = "origin"
        case track = "track"
        case detailsId = "detailsId"
    }
    
    public init(name: String, type: String, boardId: Int, stopId: Int, dateTime: String, origin: String, track: String?, detailsId: String) {
        self.name = name
        self.type = type
        self.boardId = boardId
        self.stopId = stopId
        self.dateTime = dateTime
        self.origin = origin
        self.track = track ?? "89"
        self.detailsId = detailsId
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.track = try container.decodeIfPresent(String.self, forKey: .track) ?? "unknown"
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? "unknown"
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? "unknown"
        self.boardId = try container.decodeIfPresent(Int.self, forKey: .boardId) ?? 0
        self.stopId = try container.decodeIfPresent(Int.self, forKey: .stopId) ?? 0
        let dateTime = try container.decodeIfPresent(String.self, forKey: .dateTime) ?? ""
           
        let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        dateFormatter.timeZone = NSTimeZone.local as TimeZone?
        let date = dateFormatter.date(from: dateTime)

           // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date!)
        
        self.dateTime = timeStamp
        self.origin = try container.decodeIfPresent(String.self, forKey: .origin) ?? ""
        
        self.detailsId = try container.decodeIfPresent(String.self, forKey: .detailsId) ?? ""
        
    }
}

public typealias Trains = [Train]
