//
//  MarudorStationCoordinates.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 06.01.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation

public class MarudorStationCoordinates:Codable, Identifiable{
    
    var lat: Double;
    
    var lng: Double;
    
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lng = "lng"
              
            
           }
    public init(lat:Double, lng:Double){
        self.lat = lat;
        self.lng = lng;
        
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lat = try container.decodeIfPresent(Double.self, forKey: .lat) ??
        0.01;
       self.lng = try container.decodeIfPresent(Double.self, forKey: .lng) ??
       0.01;
        
        
    }
    
}
