//
//  MarudorStop.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 14.12.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

public class MarudorStop:Codable, Identifiable{
    
    var station: MarudorStation;
    var arrival: MarudorArrival;
    var departure:MarudorDeparture;
    enum CodingKeys: String, CodingKey {
               case station = "station"
               case arrival = "arrival"
              case departure = "departure"
              
            
           }
    public init(station:MarudorStation, arrival:MarudorArrival, departure:MarudorDeparture){
        self.station = station;
        self.arrival = arrival;
        self.departure = departure
        
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.station = try container.decodeIfPresent(MarudorStation.self, forKey: .station) ??
            MarudorStation(coords: MarudorStationCoordinates(lat: 0,lng: 0), title: "");
        
       
        self.arrival = try container.decodeIfPresent(MarudorArrival.self, forKey: .arrival) ??
             MarudorArrival(scheduledTime: "2021-09-28T17:16:00.000Z" , time: "2021-09-28T17:13:00.000Z" , platform: "NO", scheduledPlatform: "no", hidden: false, cancelled: false, delay: 0);
         
        
                self.departure = try container.decodeIfPresent(MarudorDeparture.self, forKey: .departure) ??
                     MarudorDeparture(scheduledTime: "2021-09-28T17:15:00.000Z" , time: "2021-09-28T17:13:00.000Z" , platform: "NO", scheduledPlatform: "no", hidden: false, cancelled: false, delay: 0);
                 
         
        
    }
    
}

public typealias MarudorStops = [MarudorStop]
