//
//  JourneyDetailModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 12.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

public class JourneyDetail: Codable, Identifiable{
    /*
     {
       "stopId": 8000274,
       "stopName": "Neuss Hbf",
       "lat": "51.204355",
       "lon": "6.684523",
       "arrTime": "22:55",
       "depTime": "22:58",
       "train": "ICE 949",
       "type": "ICE",
       "operator": "DB",
       "notes": [
         {
           "key": "CK",
           "priority": "200",
           "text": "Komfort Check-in möglich  http://bahn.de/komfortcheckin"
         }
       ]
     }
     */
    public var stopId: Int
    public var stopName: String
    public var lat: String
    public var lon: String
    public var arrTime: String
    public var depTime: String
    
    enum CodingKeys: String, CodingKey {
        case stopId = "stopId"
        case stopName = "stopName"
        case lat = "lat"
        case lon = "lon"
        
        case arrTime = "arrTime"
        case depTime = "depTime"
        
    }
    
    public init(stopId: Int, stopName: String, lat: String,  lon: String, arrTime: String, depTime: String) {
        self.stopName = stopName
        self.lat = lat
        self.lon = lon
        self.stopId = stopId
        self.arrTime = arrTime
        self.depTime = depTime
        
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.stopName = try container.decodeIfPresent(String.self, forKey: .stopName) ?? "unknown"
        self.lat = try container.decodeIfPresent(String.self, forKey: .lat) ?? "unknown"
        self.lon = try container.decodeIfPresent(String.self, forKey: .lon) ?? "unknown"
        
        self.stopId = try container.decodeIfPresent(Int.self, forKey: .stopId) ?? 0
        self.arrTime = try container.decodeIfPresent(String.self, forKey: .arrTime) ?? ""
           self.depTime = try container.decodeIfPresent(String.self, forKey: .depTime) ?? ""

        
    }
}

public typealias JourneyDetails = [JourneyDetail]
