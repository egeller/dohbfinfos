//
//  MarudorArrival.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 03.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

public class MarudorArrival:Codable, Identifiable{
    //"arrival":{"scheduledTime":1572812340000,"time":1572812340000,"platform":"11","scheduledPlatform":"11","hidden":false,"cancelled":false,"delay":0}
    public var scheduledTime:Int
    public var time:Int
    public var platform: String
    public var scheduledPlatform: String
    public var hidden: Bool
    public var cancelled: Bool
    public var delay: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case scheduledTime = "scheduledTime"
        case time = "time"
        case platform = "platform"
        case scheduledPlatform = "scheduledPlatform"
        case delay = "delay"
         case hidden = "hidden"
        
         case cancelled = "cancelled"
     
    }
    
    public init(scheduledTime:String, time:String, platform: String, scheduledPlatform: String, hidden: Bool, cancelled: Bool, delay: Int) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        let datetime = formatter.date(from: scheduledTime)
        let datetime2 = formatter.date(from: time)

        self.scheduledTime = Int(datetime?.timeIntervalSince1970 ?? 0)

        //.scheduledTime = scheduledTime
        self.platform = platform
        self.scheduledPlatform = scheduledPlatform
        self.hidden = hidden
        self.cancelled = cancelled
        self.delay = delay
        self.time = Int(datetime2?.timeIntervalSince1970 ?? 0)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss Z"


        let scheduledTime:String  = try container.decodeIfPresent(String.self, forKey: .scheduledTime) ?? ""
        let datetime = formatter.date(from: scheduledTime)

        self.scheduledTime = Int(datetime?.timeIntervalSince1970 ?? 0)
        
        let time:String = try container.decodeIfPresent(String.self, forKey: .time) ?? ""
        let datetime2 = formatter.date(from: time)

        
        self.time  = Int(datetime2?.timeIntervalSince1970 ?? 0)
        //self.scheduledTime  = try container.decodeIfPresent(Int.self, forKey: .scheduledTime) ?? 0
        //self.time  = try container.decodeIfPresent(Int.self, forKey: .time) ?? 0
        self.platform = try container.decodeIfPresent(String.self, forKey: .platform) ?? "unknown"
        self.scheduledPlatform = try container.decodeIfPresent(String.self, forKey: .scheduledPlatform) ?? "unknown"
        self.hidden = try container.decodeIfPresent(Bool.self, forKey: .hidden) ?? true
        self.cancelled  = try container.decodeIfPresent(Bool.self, forKey: .cancelled ) ?? false
         self.delay  = try container.decodeIfPresent(Int.self, forKey: .delay) ?? 0
        
    }
    
}
