//
//  StationModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 02.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import SwiftUI

import Combine

public class Station: Codable, Identifiable{
    
    public let stationnumber: Int
    public let name:String
    public let facilities: [Facility]
    
    
        
        enum CodingKeys: String, CodingKey {
            case stationnumber = "stationnumber"
            case name = "name"
            case facilities = "facilities"
            
        }
        
    public init(stationnumber: Int,  name:String, facilities: [Facility]) {
           
            self.name = name
            self.facilities = facilities
            self.stationnumber = stationnumber
        }
        
        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.facilities = try container.decodeIfPresent([Facility].self, forKey: .facilities) ?? []
            self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? "unknown"
            
            self.stationnumber = try container.decodeIfPresent(Int.self, forKey: .stationnumber) ?? 0
            
                    
            
        }
    
    
}


