//
//  MarudorQos.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 05.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

public class MarudorQos:Codable, Identifiable{
    public var text:String
    public var time:Int
    
    
    enum CodingKeys: String, CodingKey {
        case text = "text"
        case time = "time"
      
     
    }
    
    public init(text:String, time:Int) {
        self.text = text
    
        self.time = time
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.text  = try container.decodeIfPresent(String.self, forKey: .text) ?? ""
        self.time  = try container.decodeIfPresent(Int.self, forKey: .time) ?? 0
       
        
    }
    
    
}

public typealias MarudorQoses = [MarudorQos]
