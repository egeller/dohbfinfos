//
//  MarudorJourneyDetailModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 11.12.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

public class MarudorJourneyDetail:Codable, Identifiable{
    
    var stops:MarudorStops
    
    enum CodingKeys: String, CodingKey {
        case stops = "stops"
        
            
           }
    public init(stops:MarudorStops){
        self.stops = stops;
       
        
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.stops = try container.decodeIfPresent(MarudorStops.self, forKey: .stops) ?? []
        
       
        
        
    }
    
    
}
