//
//  FacilityModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 02.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI

/*
    
    {"equipmentnumber":10438490,"type":"ESCALATOR","description":"zu U-Bahn Gleis","geocoordX":7.4600625,"geocoordY":51.51773305,"state":"ACTIVE","stateExplanation":"available","stationnumber":1289}
    */

public class Facility: Codable, Identifiable{
   
    public let equipmentnumber: Int
        public let type: String
        public let description: String
        public let geocoordX: Double
        public let geocoordY: Double
        public let state: String
    
        public let stateExplanation: String
        public let stationnumber: Int
        
        
        enum CodingKeys: String, CodingKey {
            case equipmentnumber = "equipmentnumber"
            case type = "type"
            case description = "description"
            case geocoordX = "geocoordX"
            case geocoordY = "geocoordY"
            case state = "state"
            case stationnumber = "stationnumber"
            case stateExplanation = "stateExplanation"
        }
        
    public init(equipmentnumber: Int, type: String, description: String, geocoordX: Double, geocoordY: Double, state: String, stateExplanation:String, stationnumber: Int) {
            self.equipmentnumber = equipmentnumber
            self.type = type
            self.description = description
            self.state = state
            self.geocoordX = geocoordX
            self.geocoordY = geocoordY
            self.stateExplanation = stateExplanation
            self.stationnumber = stationnumber
        }
        
        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.equipmentnumber = try container.decodeIfPresent(Int.self, forKey: .equipmentnumber) ?? 0
            let typeText = try container.decodeIfPresent(String.self, forKey: .type) ?? "unknown"
            if typeText == "ELEVATOR"{
                self.type = "Aufzug"
            }else if typeText == "ESCALATOR" {
               self.type = "Fahrtreppe"
            }else{
                self.type = typeText
            }
            self.state = try container.decodeIfPresent(String.self, forKey: .state) ?? "unknown"
            self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? "unknown"
            self.geocoordX = try container.decodeIfPresent(Double.self, forKey: .geocoordX) ?? 0
            self.geocoordY = try container.decodeIfPresent(Double.self, forKey: .geocoordY) ?? 0
            self.stationnumber = try container.decodeIfPresent(Int.self, forKey: .stationnumber) ?? 0
            let stateExplanationText = try container.decodeIfPresent(String.self, forKey: .stateExplanation) ?? ""
            if stateExplanationText == "available"{
                self.stateExplanation = "verfügbar"
            }else{
                self.stateExplanation = "nicht verfügbar"
            }
               
         
            
        }
    }

    public typealias Facilities = [Facility]
    

