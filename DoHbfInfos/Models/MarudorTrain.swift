//
//  MarudorTrain.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 01.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

public class MarudorTrain:Codable, Identifiable{
    public let name:String
    public let trainCategory:String
    public let number:String
    
    enum CodingKeys: String, CodingKey {
               case name = "name"
               case trainCategory = "type"
               case number = "number"
            
           }
    public init(name:String, trainCategory:String, number:String){
        self.name = name
        self.number = number
        self.trainCategory = trainCategory
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ??
            "unknown"
        self.number = try container.decodeIfPresent(String.self, forKey: .number) ??
            "unknown"
        self.trainCategory = try container.decodeIfPresent(String.self, forKey: .trainCategory) ??
        "unknown"
        
    }
    
    
}
