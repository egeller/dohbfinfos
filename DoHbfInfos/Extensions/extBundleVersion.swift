//
//  extBundleVersion.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 04.01.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import Foundation

/*
 
 Versionsnummer + Buildnummer
 @see https://thoughtbot.com/blog/making-your-version-number-super-visible
 */

extension Bundle {
  private var releaseVersionNumber: String {
    return infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
  }

  private var buildVersionNumber: String {
    return infoDictionary?["CFBundleVersion"] as? String ?? ""
  }

  var bundleID: String {
    return Bundle.main.bundleIdentifier?.lowercased() ?? ""
  }

  var versionString: String {
    var scheme = ""

    // If you use different bundle IDs for different environments, code like this is helpful:
    if bundleID.contains(".dev") {
      scheme = "Dev"
    } else if bundleID.contains(".staging") {
      scheme = "Staging"
    }

    let returnValue = "Version \(releaseVersionNumber) (\(buildVersionNumber)) \(scheme)"

    return returnValue.trimmingCharacters(in: .whitespacesAndNewlines)
  }
}
