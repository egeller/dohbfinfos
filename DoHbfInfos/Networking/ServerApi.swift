//
//  ServerApi.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 28.09.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import Alamofire

class ServerApi{
    public var arrivalsUrl = "http://infru.de/bahn/arrivals.json";
    public var departuresUrl = "http://infru.de/bahn/departures.json";
    public var facilitiesUrl = "http://infru.de/bahn/facilities.json";
    
    var apiDataStringDidReceivedClosure: (() -> ())?
    
    
    /**
      Request with Alamofire
      
      - Parameter url: full url for request
      */
     func getRequest(url: String) {
       // Alamofire 5.0 - AF!
       AF.request(url).responseJSON { response in
        print(response.request!)   // original url request
        print(response.response!) // http url response
           print(response.result)  // response serialization result
           if let json = response.value {
               print("JSON: \(json)") // serialized json response
           }
       }
     }
}
