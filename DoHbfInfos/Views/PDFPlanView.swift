//
//  PDFView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 04.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct PDFPlanView: View {
    var url: URL
    var body: some View {
        PDFKitRepresentedView(url)
    }
}

#if DEBUG
let pdfUrl =  Bundle.main.url(forResource:  "Dortmund-Hbf_locationPdf-data" , withExtension: "pdf")!
struct PDFView_Previews: PreviewProvider {
    static var previews: some View {
        PDFPlanView(url: pdfUrl)
    }
}
#endif
