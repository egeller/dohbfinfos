//
//  TrainList.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 29.09.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//
import Foundation
import SwiftUI


struct TrainList: View {
    var trains: Trains
    var body: some View {
        List(self.trains) { train in
            TrainCell(trainCellViewModel: TrainCellViewModel(train: train))
        }
            
    }
}

#if DEBUG
let trains = [Train(name: "ICE 1221", type: "ICE", boardId: 120, stopId: 120, dateTime: "2019-09-28T22:28", origin: "München", track: "22", detailsId : "")]


struct TrainList_Previews: PreviewProvider {
    static var previews: some View {
        TrainList(trains: trains)
    }
}

#endif
