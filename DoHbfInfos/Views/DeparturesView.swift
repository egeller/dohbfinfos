//
//  DeparturesView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 01.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct DeparturesView: View {
    var departures: MarudorLines
    var error : String
    @ObservedObject var departuresViewModel: DeparturesViewModel
    @State private var show_modal: Bool = false
    var alert: Alert {
        Alert(title: Text("Verbindungsfehler"), message: Text("Bitte überprüfen Sie Verbindung zum Internet"), dismissButton: .default(Text("OK")){
            self.departuresViewModel.clearError()
            })
    }
    
    
    
    var body: some View {
        NavigationView{
            List(
                self.departuresViewModel.dataContainer.departures) { departure in
                NavigationLink(destination: MarudorLineDetailView(marudorLineViewModel: MarudorLineViewModel(marudorLine: departure))) {
                    
                   MarudorLineView(marudorLineViewModel: MarudorLineViewModel(marudorLine: departure))
                    
                }
                }.navigationBarTitle("Züge").navigationBarItems(trailing:
                    Button(action: {
                        print("lade neu")
                        self.departuresViewModel.load()
                    }) {
                        HStack{
                            Image(systemName: "arrow.clockwise")
                            .font(.subheadline)
                            .padding()
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .clipShape(Circle())
                            
                            
                        }
                    }.alert(isPresented: self.$departuresViewModel.isError, content: { self.alert
                        
                    })
                )
            
            
            Group{
                if self.departures.count > 0 {
            MarudorLineDetailView(marudorLineViewModel: MarudorLineViewModel(marudorLine: self.departures[0]))
            .navigationBarTitle("Detail")
                }else{
                    VStack{
                        Text(self.error)
                    }
                }
            }
        }.navigationViewStyle(StackNavigationViewStyle())
        .padding()
    }
    
}

#if DEBUG
struct DeparturesView_Previews: PreviewProvider {
    
    static var previews: some View {
        DeparturesView(departures: [], error: "", departuresViewModel: departuresViewModel)
    }
}
#endif
