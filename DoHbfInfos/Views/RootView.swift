//
//  RootView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 09.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct RootView: View {
    
       @State private var selection = 0
       @State private var richtung = 0
       @ObservedObject var viewModel: TrainListViewModel
       @ObservedObject var stationViewModel: StationViewModel
       @ObservedObject var departuresViewModel: DeparturesViewModel
    let timer = Timer.publish(every: 60, on: .main, in: .common).autoconnect()
    var alert: Alert {
        Alert(title: Text("Verbindungsfehler"), message: Text("Bitte überprüfen Sie Verbindung zum Internet"), dismissButton: .default(Text("OK")))
    }
    
       var body: some View {
           TabView(selection: $selection){
               VStack{
               
                    //Text("Dortmund Hbf").font(Font.headline)
                    StationView(station: self.stationViewModel.station).onReceive(timer) { time in
                        
                        print("timer"); self.stationViewModel.load()
                    }
                    
                }.font(.title)
                
                
                    .tabItem {
                        VStack {
                            Image(systemName: "house.fill")
                            //Text("Aufzüge und Fahrtreppen")
                            
                        }
                    }
                    .tag(0)
           /*
            VStack {
                   Text("Timetable")
                   Picker(selection: $richtung, label: Text("What is your favorite color?")) {
                       Text("Ankunft").tag(0)
                       Text("Abfahrt").tag(1)
                       
                   }.pickerStyle(SegmentedPickerStyle())

                   //Text("Value: \(richtung)")
                   Group(){
                       if richtung == 0 {
                           TrainList(trains: self.viewModel.trains)
                           
                       }else{
                          TrainList(trains: self.viewModel.departures)
                       }
                   }.gesture(
                       DragGesture(minimumDistance: 3)
                           .onEnded { _ in
                               print("pressed!")
                               self.viewModel.load()
                           }
                   )
                   
                   
               
                   
                   
                   Spacer()
               }.font(.title)
                   .tabItem {
                         VStack {
                           Image(systemName: "tram.fill")
                                         }
               }.tag(1)*/
            VStack() {
                 DeparturesView(departures:   departuresViewModel.dataContainer.departures, error: departuresViewModel.error, departuresViewModel: departuresViewModel).gesture(DragGesture(minimumDistance: 20)
                .onEnded { _ in
                    print("pressed!")
                    self.departuresViewModel.load()
                 })
                
                
            }.font(.title)
                .tabItem {
                      VStack {
                        Image(systemName: "tram.fill")
                                          
                                      }
            }.tag(2)
            
            VStack {
                InfoView()
            }.font(.title)
                .tabItem {
                      VStack {
                        Image(systemName: "info.circle.fill")
                                          
                                      }
            }.tag(3)
               
           }
       }
}

#if DEBUG
let viewModelR = TrainListViewModel();
let stationViewModelR = StationViewModel();
struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView(viewModel: viewModelR, stationViewModel: stationViewModelR, departuresViewModel: departuresViewModel)
    }
}
#endif
