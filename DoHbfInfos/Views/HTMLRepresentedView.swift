//
//  HTMLRepresentedView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 15.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit
import WebKit

// Small class to hold variables that we'll use in the View body
class observable: ObservableObject {
    @Published var observation:NSKeyValueObservation?
    @Published var loggedIn = false
}

struct HTMLRepresentedView: UIViewRepresentable{
    
    
    
    @ObservedObject var observe = observable()
    
    let infoWebView = WKWebView();
   

    func makeUIView(context: UIViewRepresentableContext<HTMLRepresentedView>) -> UIView {
       
        
        guard let path = Bundle.main.path(forResource: "info", ofType: "html") else { return infoWebView }
           let infoHtmlString = try! String(contentsOfFile: path).trimmingCharacters(in: .whitespacesAndNewlines);
           
           
        
        infoWebView.loadHTMLString(infoHtmlString, baseURL: nil);
        return infoWebView;
    }

    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<HTMLRepresentedView>) {
        // Update the view.
        /*
        print("will update")
        
         // Set up our key-value observer - we're checking for WKWebView.title changes here
                 // which indicates a new page has loaded.
        observe.observation = infoWebView.observe(\WKWebView.url, options: .new) { view, change in
            print(view.url ?? "def")
            UIApplication.shared.open(view.url!)
                guard let path = Bundle.main.path(forResource: "info", ofType: "html") else { return  }
                   let infoHtmlString = try! String(contentsOfFile: path).trimmingCharacters(in: .whitespacesAndNewlines);
                   
                   
                
            self.infoWebView.loadHTMLString(infoHtmlString, baseURL: nil);
                   
               }
        
        */
       
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
      //SEE: https://gist.github.com/kevenbauke/d449718a5f268ee843f286db88f137cc
      if navigationAction.navigationType == .linkActivated {
        guard let url = navigationAction.request.url else {
          decisionHandler(.allow)
          return
        }
        
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        //if ["tel", "sms", "facetime"].contains(url.scheme) && UIApplication.shared.canOpenURL(url) {
        if components?.scheme == "http" || components?.scheme == "https" || (["tel", "sms", "mailto"].contains(url.scheme ) && UIApplication.shared.canOpenURL(url))
        {
          UIApplication.shared.open(url)
          decisionHandler(.cancel)
        } else {
          decisionHandler(.allow)
        }
      } else {
        decisionHandler(.allow)
      }
    }
}

