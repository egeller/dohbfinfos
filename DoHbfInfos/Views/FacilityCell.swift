//
//  FacilityCell.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 03.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI
import Combine

struct FacilityCell: View {
    var facilityCellViewModel: FacilityCellViewModel
    var body: some View {
        HStack(){
            Text(facilityCellViewModel.getType()).font(Font.subheadline).padding()
            Text(facilityCellViewModel.getDescription()).font(Font.body).padding()
            Text(facilityCellViewModel.getStateExplanation()).font(Font.body).padding()
        }
        
    }
}

#if DEBUG

let facility = Facility(equipmentnumber: 1, type: "Elevator", description: "One Elevator", geocoordX: 0, geocoordY:0, state: "active", stateExplanation:"Available", stationnumber: 8000080)

let cellViewModel = FacilityCellViewModel(facility: facility)

struct FacilityCell_Previews: PreviewProvider {
    static var previews: some View {
        FacilityCell(facilityCellViewModel: cellViewModel)
    }
}
#endif
