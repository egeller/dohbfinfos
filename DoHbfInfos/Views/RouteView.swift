//
//  RouteView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 10.01.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI

struct RouteView: View {
    var dismiss: () -> ()
    var stops: MarudorStops
    var body: some View {
        VStack{
            Button(action: dismiss) {
                Text("Schliessen")
            }
            MapRepresentedView(stops: stops)
            Spacer()
        }
    }
}
#if DEBUG

struct RouteView_Previews: PreviewProvider {
    
    static var previews: some View {
        RouteView(dismiss: { }, stops: [])
    }
}
#endif
