//
//  TestLineView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 06.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct TestLineView: View {
    @Environment(\.presentationMode) var presentationMode
       
      var marudorLineViewModel: MarudorLineViewModel;
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TestLineView_Previews: PreviewProvider {
    static var previews: some View {
        TestLineView(marudorLineViewModel:  MarudorLineViewModel(marudorLine: MarudorLine(initialDeparture: "2021-09-19T08:03:00.000Z", scheduledDestination: "Dortmund", routes: [], train: MarudorTrain( name: "Testtrain", trainCategory: "Test",  number: "Test" ), destination: "Dortmund", platform: "11", messages: MarudorMessage(delays: [], qoses: []), departure:  MarudorDeparture(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "9", scheduledPlatform: "9", hidden: false, cancelled: false, delay: 5),  arrival: MarudorArrival(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "9", scheduledPlatform: "9", hidden: false, cancelled: false, delay: 5))))
    }
}
