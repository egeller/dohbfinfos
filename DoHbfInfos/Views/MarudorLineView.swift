//
//  MarudorLineView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 02.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct MarudorLineView: View {
    var marudorLineViewModel:MarudorLineViewModel
    
    var body: some View {
        VStack{
            HStack{
            Text(marudorLineViewModel.getTrainName()).padding().font(.subheadline)
            Text(marudorLineViewModel.getDestination()).padding().font(.subheadline)
            }
        HStack {
            VStack{
                
                HStack{
                    Text("Geplant:").padding().font(.subheadline)
                    Text(marudorLineViewModel.getArrivalScheduledTime()).padding().font(.subheadline)
                     Text(marudorLineViewModel.getDepartureScheduledTime()).padding().font(.subheadline)
                }
                HStack{
                    Text("Erwartet:").padding().font(.subheadline)
                    Text(marudorLineViewModel.getArrivalTime()).padding().font(.subheadline)
                   
                    Text(marudorLineViewModel.getDepartureTime()).padding().font(.subheadline)
                }
            }
           
            
            
        }
            
                       
        }.padding()
        
    }
}
 #if DEBUG

struct MarudorLineView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        
        MarudorLineView(marudorLineViewModel: MarudorLineViewModel(marudorLine:  MarudorLine(initialDeparture: "2021-09-19T08:03:00.000Z", scheduledDestination: "Dortmund", routes:[], train:MarudorTrain(name: "RE45", trainCategory: "RE", number: "45"), destination: "Dortmund", platform: "11", messages: MarudorMessage(delays: [], qoses: []), departure:  MarudorDeparture(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "9", scheduledPlatform: "9", hidden: false, cancelled: false, delay: 5), arrival: MarudorArrival(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "9", scheduledPlatform: "9", hidden: false, cancelled: false, delay: 5) ) ))
    }
    
}
#endif
