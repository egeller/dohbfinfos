//
//  MapRepresentedView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 10.01.20.
//  Copyright © 2020 Elena Geller. All rights reserved.
//

import SwiftUI
import MapKit

struct MapRepresentedView: UIViewRepresentable{
    
    var stops: MarudorStops
    // 1.
    func makeUIView(context: UIViewRepresentableContext<MapRepresentedView>) -> MKMapView {
        MKMapView(frame: .zero)
    }
    
    func makeCoordinator() -> MapViewCoordinator{
         return MapViewCoordinator(self)
    }
    
    
    // 2.
    func updateUIView(_ uiView: MKMapView, context:Context) {
        uiView.delegate = context.coordinator
        // 3.
        let location = CLLocationCoordinate2D(latitude: 51.1656914,
            longitude: 10.4515257)
        // 4.
        let span = MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 5)
        let region = MKCoordinateRegion(center: location, span: span)
        uiView.setRegion(region, animated: true)
        var coords: [CLLocationCoordinate2D] = []
        for stop in stops{
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: stop.station.coordinates.lat,
                                                           longitude: stop.station.coordinates.lng)
            coords.append(annotation.coordinate)
            annotation.title = stop.station.title
            
            let arrivalTime = stop.arrival.time
            let departureTime = stop.departure.time
            annotation.subtitle = getIntervalFormatted(dateFormattedVon: getTimeFormatted(datetime: arrivalTime) , dateFormattedBis: getTimeFormatted(datetime: departureTime))
                   uiView.addAnnotation(annotation)
        }
        
        
        
        let polyline = MKPolyline(coordinates: &coords, count: coords.count)
        uiView.addOverlay(polyline)
        
     
    }
    
    func getIntervalFormatted(dateFormattedVon:String, dateFormattedBis:String)->String{
        if dateFormattedVon == ""{
            return dateFormattedBis
        }
        if dateFormattedBis == ""{
            return dateFormattedVon
        }
        
        return dateFormattedVon + " - " + dateFormattedBis
        
    }
    
    func getTimeFormatted(datetime:Int)->String{
        if datetime == 0{
           return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(datetime))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))d
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate;
    }
    
    /*
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay.isKind(of: MKPolyline.self) {
            // draw the track
            let polyLine = overlay
            let polyLineRenderer = MKPolylineRenderer(overlay: polyLine)
            polyLineRenderer.strokeColor = UIColor.blue
            polyLineRenderer.lineWidth = 3.0
            
            return polyLineRenderer
        }
        
        return MKPolylineRenderer()
    }*/
    
}




class MapViewCoordinator: NSObject, MKMapViewDelegate {
        var mapViewController: MapRepresentedView
        
        init(_ control: MapRepresentedView) {
          self.mapViewController = control
        }
    
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        if let multiPolyline = overlay as? MKMultiPolyline{
            let polylineRenderer = MKMultiPolylineRenderer(multiPolyline: multiPolyline)
            polylineRenderer.strokeColor = UIColor.blue.withAlphaComponent(0.5)
            polylineRenderer.lineWidth = 5
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}



struct MapRepresentedView_Previews: PreviewProvider {
    static var previews: some View {
        MapRepresentedView(stops: [])
    }
}
