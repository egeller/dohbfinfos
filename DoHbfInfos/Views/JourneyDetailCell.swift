//
//  JourneyDetailCell.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 13.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct JourneyDetailCell: View {
    var journeyDetailViewModel: JourneyDetailViewModel
    var body: some View {
        HStack{
            Text(journeyDetailViewModel.getStopName()).font(Font.headline).padding()
            Text(journeyDetailViewModel.getArrTime()).font(Font.subheadline).padding()
            Text(journeyDetailViewModel.getDepTime()).font(Font.subheadline).padding()
            
        }
    }
}

#if DEBUG
var journeyDetailForCell = JourneyDetail(stopId: 800080, stopName: "Köln", lat: "",  lon: "", arrTime: "22:15", depTime: "22:30")
var journeyDetailViewModelForCell = JourneyDetailViewModel(journeyDetail: journeyDetailForCell)
struct JourneyDetailCell_Previews: PreviewProvider {
    static var previews: some View {
        JourneyDetailCell(journeyDetailViewModel: journeyDetailViewModelForCell)
    }
}
#endif
