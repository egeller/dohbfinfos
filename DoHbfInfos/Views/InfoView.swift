//
//  InfoView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 15.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct InfoView: View {
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text(self.getVersion()).padding().font(.subheadline).multilineTextAlignment(.trailing)
            }
        HTMLRepresentedView()
        }
    }
    
    func getVersion()->String{
        return Bundle.main.versionString;
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}
