//
//  TrainCell.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 29.09.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI
import Combine

struct TrainCell: View {
     var trainCellViewModel: TrainCellViewModel
    @State private var show_modal: Bool = false
    var body: some View {
        HStack(){
            Text(self.trainCellViewModel.getDateTime()).font(Font.subheadline).padding()
            Text(self.trainCellViewModel.getName()).font(Font.caption).padding()
            Text("Gleis " + self.trainCellViewModel.getTrack()).font(Font.body).padding()
            Button(action: {
                print("Button Pushed")
                self.show_modal = true
            }) {
                Text("Details").font(Font.subheadline)
            }.sheet(isPresented: self.$show_modal) {
                JourneyView(detailId: self.trainCellViewModel.getDetailsId())
                }
            
            
        }
        
    }
}
#if DEBUG

let train = Train(name: "ICE 1221", type: "ICE", boardId: 120, stopId: 120, dateTime: "2019-09-28T22:28", origin: "München", track: "22", detailsId : "")

let model = TrainCellViewModel(train: train)

struct TrainCell_Previews: PreviewProvider {
    static var previews: some View {
        TrainCell(trainCellViewModel: model)
    }
}
#endif
