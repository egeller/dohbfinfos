//
//  MarudorLineDetailView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 03.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct MarudorLineDetailView: View {
    @Environment(\.presentationMode) var presentationMode
    
   var marudorLineViewModel: MarudorLineViewModel;
    var marudorJourneyDetail: MarudorJourneyDetail = MarudorJourneyDetail(stops: [])
    @State var showingRoute = false

    
    var body: some View {
            ScrollView{
            VStack{
                VStack{
                    Button(action: {
                        self.showingRoute.toggle()
                    }) {
                        Text("Auf die Karte Zeigen").padding().font(.subheadline)
                    }.sheet(isPresented: $showingRoute) {
                        RouteView(dismiss: { self.showingRoute = false }, stops: self.marudorJourneyDetail.stops);
                        
                    }
                    
                     
                     HStack{
                         Text("Geplant:").padding().font(.subheadline)
                         Text(marudorLineViewModel.getArrivalScheduledTime()).padding().font(.subheadline)
                            
                        
                          Text(marudorLineViewModel.getDepartureScheduledTime()).padding().font(.subheadline)
                           
                        
                     }
                     HStack{
                         Text("Erwartet:").padding().font(.subheadline)
                         Text(marudorLineViewModel.getArrivalTime()).padding().font(.subheadline)
                        
                        
                        
                         Text(marudorLineViewModel.getDepartureTime()).padding().font(.subheadline)
                       
                        
                     }
                    HStack{
                        Text("Verspätung:").padding().font(.subheadline)
                        Text(marudorLineViewModel.getArrivalDelayAsString()).padding().font(.subheadline).foregroundColor(.red)
                         Text(marudorLineViewModel.getDepartureDelayAsString()).padding().font(.subheadline).foregroundColor(.red)
                    }
                   
                 }.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
                
            
                
            
                HStack {
                    
                    Text(marudorLineViewModel.getTrainName()).padding().font(.title)
                        
                    
                
                }
                HStack(){
                    Text(marudorLineViewModel.getDestination())
                    Text("Gleis " + marudorLineViewModel.getPlatform())
                }
                VStack(){
                    ForEach(marudorLineViewModel.getRoutes()){
                        route in Text(route.name).font(.subheadline)
                    }
                }
                VStack(){
                    ForEach(marudorLineViewModel.getDelays()){
                        delay in Text(delay.text).font(.subheadline).foregroundColor(.red)
                    }
                }
                VStack(){
                                   ForEach(marudorLineViewModel.getQoses()){
                                       qos in Text(qos.text).font(.subheadline).foregroundColor(.gray)
                                   }
                }.onAppear(perform: getRouteDetails)
                Spacer()
                }
               
                
        }
        }
   func getRouteDetails(){
        let trainCode = self.marudorLineViewModel.getTrainCode()
        
        print("https://marudor.de/api/hafas/v2/details/" + trainCode)
        guard let url = URL(string: "https://marudor.de/api/hafas/v2/details/" + trainCode) else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            // step 4
           if let data = data {
            let resp =  try? JSONSerialization.jsonObject(with: data, options:[])
            print(resp) //JSONSerialization
                if let decodedResponse = try? JSONDecoder().decode(MarudorJourneyDetail.self, from: data) {
                    // we have good data – go back to the main thread
                    print(decodedResponse)
                    DispatchQueue.main.async {
                        self.marudorJourneyDetail.stops = decodedResponse.stops
                    }
                }else{
                    print("decode failed: \(error?.localizedDescription ?? "Unknown error")")
            }

                    // everything is good, so we can exit
                    return
                }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
        }.resume()
       
    }
    
    }


struct MarudorLineDetailView_Previews: PreviewProvider {
    
    
    static var previews: some View {
        MarudorLineDetailView(marudorLineViewModel:  MarudorLineViewModel(marudorLine: MarudorLine(initialDeparture: "2021-09-19T08:03:00.000Z" , scheduledDestination: "Dortmund", routes: [], train: MarudorTrain( name: "Testtrain", trainCategory: "Test",  number: "Test" ), destination: "Dortmund", platform: "11", messages: MarudorMessage(delays: [], qoses: []), departure:  MarudorDeparture(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "9", scheduledPlatform: "9", hidden: false, cancelled: false, delay: 5), arrival: MarudorArrival(scheduledTime: "2021-09-28T17:13:00.000Z", time: "2021-09-28T17:13:00.000Z", platform: "9", scheduledPlatform: "9", hidden: false, cancelled: false, delay: 5) )))
    }
}
