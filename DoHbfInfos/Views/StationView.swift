//
//  StationView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 03.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

import UIKit

struct StationView: View {
    var station: Station
    var body: some View {
        
        VStack{
           
            PDFPlanView(url:  Bundle.main.url(forResource:  "Dortmund-Hbf_locationPdf-data" , withExtension: "pdf")!)
            List(self.station.facilities) { facility in
                           FacilityCell(facilityCellViewModel: FacilityCellViewModel(facility: facility))
                              }
        
            
        
        }
    }
    
}
#if DEBUG
let testFacility = Facility(equipmentnumber: 1, type: "Elevator", description: "One Elevator", geocoordX: 0, geocoordY:0, state: "active", stateExplanation:"Available", stationnumber: 8000080)
let station = Station(stationnumber: 0,  name: "Dortmund Hbf", facilities:[testFacility]);
struct StationView_Previews: PreviewProvider {
    static var previews: some View {
        StationView(station: station)
    }
}
#endif
