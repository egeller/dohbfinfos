//
//  JourneyView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 09.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

struct JourneyView: View {
    var detailsID: String
    @Environment(\.presentationMode) var presentationMode
       
    
    @ObservedObject var journeyViewModel: JourneyViewModel;
    
    var body: some View {
        VStack{
            Button(action : {
               self.presentationMode.wrappedValue.dismiss()
            }
            ) {
                HStack(){
                    Image(systemName: "xmark.circle")
                    Text("schließen")
                }
                
            }
            Text(self.journeyViewModel.error)
        List(self.journeyViewModel.details) { journeyDetail in
            JourneyDetailCell(journeyDetailViewModel: JourneyDetailViewModel(journeyDetail: journeyDetail))
        }
        }
            
    }
    
    public init(detailId: String){
        self.detailsID = detailId
        self.journeyViewModel =  JourneyViewModel(detailId: self.detailsID);
        self.journeyViewModel.load()
    }
    
    
}

struct JourneyView_Previews: PreviewProvider {
    static var previews: some View {
        
        JourneyView(detailId: "detail")
    }
}
