//
//  MarudorLineViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 02.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

public class MarudorLineViewModel {
    
    private var marudorLine: MarudorLine
    
    public init(marudorLine: MarudorLine) {
        self.marudorLine = marudorLine
    }
    
   
    
    public func getDestination() -> String {
        return self.marudorLine.destination
    }
    
    public func getTrain() -> MarudorTrain{
        return self.marudorLine.train!
    }
    
    public func getTrainName()->String{
        return self.marudorLine.train?.name ?? "";
    }
    
    public func getTrainCode()->String{
        let type = self.marudorLine.train?.trainCategory ?? "";
        let number = self.marudorLine.train?.number ?? "";
        let code = type + " " + number
        let escaped = code.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        return escaped;
    }
    
    public func getRoutes()->MarudorRoutes{
        return self.marudorLine.routes
    }
    
    public func getRoutesNames()->String{
        var routesAsString = "";
        for route in self.marudorLine.routes {
            routesAsString += " - " + route.name;
            
        }
        return String(routesAsString.dropLast(3))
    }
    
    public func getPlatform()->String{
        return self.marudorLine.platform
    }
    
    public func getInitialDeparture()->String{
        let initDepartureAsTimestamp = self.marudorLine.initialDeparture
        
        let date = Date(timeIntervalSince1970: TimeInterval(initDepartureAsTimestamp))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
        
    }
    
    public func getDelays()->MarudorDelays{
        return self.marudorLine.messages.delays
    }
    
    public func getQoses()->MarudorQoses{
        return self.marudorLine.messages.qoses
    }
    
    public func getArrivalTime()->String{
        if self.marudorLine.arrival.time == 0{
            return "-";
        }
        let arrivalTimestamp = self.marudorLine.arrival.time
        let date = Date(timeIntervalSince1970: TimeInterval(arrivalTimestamp ))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
    }
    
    public func getDepartureTime()->String{
        if self.marudorLine.departure.time == 0{
            return "-";
        }
        let arrivalTimestamp = self.marudorLine.departure.time
        let date = Date(timeIntervalSince1970: TimeInterval(arrivalTimestamp ))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
    }
    
    
    public func getArrivalScheduledTime()->String{
        if self.marudorLine.arrival.scheduledTime == 0{
            return "-";
        }
        let arrivalTimestamp = self.marudorLine.arrival.scheduledTime
        print("arrivaltimestamp ")
        print(arrivalTimestamp)
        let date = Date(timeIntervalSince1970: TimeInterval(arrivalTimestamp ))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
    }

    public func getDepartureScheduledTime()->String{
        if self.marudorLine.departure.scheduledTime == 0{
            return "-";
        }
        let arrivalTimestamp = self.marudorLine.departure.scheduledTime
        let date = Date(timeIntervalSince1970: TimeInterval(arrivalTimestamp ))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
    }
    
    
    public func getDepartmentOrArrivalTime()->String{
        var timeToShow = self.marudorLine.departure.time
        if self.marudorLine.departure.time == 0{
            timeToShow = self.marudorLine.arrival.time
        }
        let arrivalTimestamp = timeToShow
        let date = Date(timeIntervalSince1970: TimeInterval(arrivalTimestamp ))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
    }
    
    public func getDepartmentOrArrivalScheduledTime()->String{
        var timeToShow = self.marudorLine.departure.scheduledTime
        if self.marudorLine.departure.scheduledTime == 0{
            timeToShow = self.marudorLine.arrival.scheduledTime
        }
        let arrivalTimestamp = timeToShow
        let date = Date(timeIntervalSince1970: TimeInterval(arrivalTimestamp ))
        let dateFormatter = DateFormatter()
        //let timezone = TimeZone.current.abbreviation() ?? "CET"  // get current TimeZone abbreviation or set to CET
        //dateFormatter.timeZone = TimeZone(abbreviation: timezone) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        //DateFormatter(date: .none, time: .long))
        //dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
    }
    
    public func getArrivalDelay()->Int{
        return self.marudorLine.arrival.delay
    }
    
    public func getDepartureDelay()->Int{
        return self.marudorLine.departure.delay
    }
 
    public func getArrivalDelayAsString()->String{
        let delay = self.getArrivalDelay();
        let delayAsString = String(delay);
        if delay == 0{
            return ""
        }else{
            return  delayAsString 
        }
           
       }
       
     public func getDepartureDelayAsString()->String{
           let delay = self.getDepartureDelay();
           let delayAsString = String(delay);
           if delay == 0{
               return ""
           }else{
               return  delayAsString
           }
              
       }
    
    public func getMaxDelay()->String{
        var delay:Int
        if self.getArrivalDelay() > self.getDepartureDelay(){
            delay = self.getArrivalDelay()
        }else{
            delay = self.getDepartureDelay()
        }
        
        let delayAsString = String(delay);
        if delay == 0{
            return ""
        }else{
            return "(" + delayAsString + ")"
        }
    }

    
    
    
    
   
}

