//
//  JourneyViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 14.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import SwiftUI

import Combine

public class JourneyViewModel: ObservableObject{
   
    public var detailId:String
    
    public let willChange = PassthroughSubject<JourneyViewModel, Never>()
    
    @Published var details: JourneyDetails = [JourneyDetail]() {
        didSet {
            willChange.send(self)
        }
    }
    
    @Published var error: String = "" {
        didSet {
            willChange.send(self)
        }
    }
    
    public var url =  "http://infru.de/bahn/journey.php?journey="
    
    public init(detailId: String){
        self.detailId = detailId
    }
    
    func load() {
        
        let detailUrl = self.url + self.detailId
        
        print("journey detailurl \(detailUrl)")
        
        guard let url = URL(string: detailUrl) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                guard let data = data else { return }
                print(data)
                print(String(data: data, encoding: .utf8)!)
                let details = try JSONDecoder().decode(JourneyDetails.self, from: data)
                DispatchQueue.main.async {
                    print(details)
                    self.details = details
                    self.error = ""
                }
            } catch {
                print("Failed To decode journey: ", error)
                self.error = "Auskunft nicht möglich"
            }
            }.resume()
        
    }
    
}


