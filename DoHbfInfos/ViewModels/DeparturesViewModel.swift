//
//  DeparturesViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 01.11.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import SwiftUI
import Combine

public class DeparturesViewModel:ObservableObject{
    
    public let willChange = PassthroughSubject<DeparturesViewModel, Never>()
    
    @Published var dataContainer: MarudorDataContainer = MarudorDataContainer(departures:[]) {
        didSet {
            willChange.send(self)
        }
    }
    
    @Published var error: String = "" {
        didSet {
            willChange.send(self)
        }
    }
    @Published var isError: Bool = false{
        didSet{
            willChange.send(self)
        }
    }
    public var stationId = "8000080"
    
    public var url =  "https://marudor.de/api/iris/v2/abfahrten/"
    
    public func clearError(){
        isError = true
    }
        
        public init(stationId: String){
            self.stationId = stationId
        }
        
        func load() {
            
            let detailUrl = self.url + self.stationId
            //let detailUrl = "https://bahndata.etoispania.eu/arrivals.json";
            
            print("detailurl \(detailUrl)")
            
            guard let url = URL(string: detailUrl) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                do {
                    guard let data = data else {
                        DispatchQueue.main.async {
                            self.error = "Auskunft nicht möglich. Bitte überprüfen Sie Verbindung zum Internet"}
                        self.isError = true
                        return }
                    print(data)
                    //print(String(data: data, encoding: .utf8)!)
                    let dataContainer = try JSONDecoder().decode(MarudorDataContainer.self, from: data)
                    
                    
                    DispatchQueue.main.async {
                        print(dataContainer)
                        /*
                        let unsortedDepartures = dataContainer.departures;
                        let sortedDepartures = unsortedDepartures.sorted {
                            $0.initialDeparture < $1.initialDeparture
                        }
                        //dataContainer.departures = sortedDepartures
                        print(sortedDepartures)
                         */
                        self.dataContainer = dataContainer
                        self.error = ""
                        self.isError = false
                    }
                } catch {
                    print("Failed To decode 2: ", error)
                    DispatchQueue.main.async {
                       self.error = "Auskunft nicht möglich"
                        self.isError = true
                    }
                    
                }
                }.resume()
            
        }
        
    }


