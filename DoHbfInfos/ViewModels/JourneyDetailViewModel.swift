//
//  JourneyDetailViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 13.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

public class JourneyDetailViewModel{
    
    private var journeyDetail: JourneyDetail
    
    /*
     public var stopId: Int
     public var stopName: String
     public var lat: String
     public var lon: String
     public var arrTime: String
     public var depTime: String
     */
    
    public init(journeyDetail: JourneyDetail){
        self.journeyDetail = journeyDetail
    }
    
    public func getStopId()->Int{
        return self.journeyDetail.stopId;
    }
    
    public func getStopName()->String{
        return self.journeyDetail.stopName
    }
    
    public func getLat()->String{
        return self.journeyDetail.lat;
    }
    
    public func getLon()->String{
        return self.journeyDetail.lon;
    }
    
    public func getArrTime()->String{
        return self.journeyDetail.arrTime
    }
    
    public func getDepTime()->String{
        return self.journeyDetail.depTime
    }
}
