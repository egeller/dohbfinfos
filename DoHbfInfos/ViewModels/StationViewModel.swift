//
//  FacilitiesViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 02.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import SwiftUI
import Combine

public class StationViewModel: ObservableObject {
    public let willChange = PassthroughSubject<StationViewModel, Never>()
    public var url =  "https://bahndata.etoispania.eu/facilities.json"
    
    @Published var station: Station = Station(stationnumber: 1, name: "Dortmund Hbf", facilities: []) {
        didSet {
            willChange.send(self)
        }
    }
    
   
        
    func load() {
        
        guard let url = URL(string: self.url) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                guard let data = data else { return }
                
                print(data)
                print(String(data: data, encoding: .utf8)!)
                let station = try JSONDecoder().decode(Station.self, from: data)
                DispatchQueue.main.async {
                    print(station.facilities)
                    self.station = station
                }
            } catch {
                print("Failed To decode: ", error)
            }
            }.resume()
        
    }
}


