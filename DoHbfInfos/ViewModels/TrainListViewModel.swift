//
//  TRainViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 28.09.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

import SwiftUI
import Combine

import Alamofire

public class TrainListViewModel: ObservableObject {
    public let willChange = PassthroughSubject<TrainListViewModel, Never>()
    public var url =  "https://bahndata.etoispania.eu/arrivals.json"
    
    @Published var trains: Trains = [Train]() {
        didSet {
            willChange.send(self)
        }
    }
    
    @Published var departures: Trains = [Train]() {
        didSet {
            willChange.send(self)
        }
    }
    
    func setArrivals(){
        self.url = "https://bahndata.etoispania.eu/arrivals.json"
    }
    
     func setDepartures(){
        self.url = "https://bahndata.etoispania.eu/departures.json"
     }
        
    func load() {
        
        setArrivals();
        guard let url = URL(string: self.url) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                guard let data = data else { return }
                print(data)
                print(String(data: data, encoding: .utf8)!)
                let trains = try JSONDecoder().decode(Trains.self, from: data)
                DispatchQueue.main.async {
                    print(trains)
                    self.trains = trains
                }
            } catch {
                print("Failed To decode trains 1: ", error)
            }
            }.resume()
        setDepartures()
        guard let urlDepartures = URL(string: self.url) else { return }
        URLSession.shared.dataTask(with: urlDepartures) { (data, response, error) in
            do {
                guard let data = data else { return }
                print(data)
                print(String(data: data, encoding: .utf8)!)
                let trains = try JSONDecoder().decode(Trains.self, from: data)
                DispatchQueue.main.async {
                    print(trains)
                    self.departures = trains
                }
            } catch {
                print("Failed To decode trains: ", error)
            }
            }.resume()
    }
}

