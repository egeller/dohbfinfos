//
//  FacilityViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 02.10.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation

public class FacilityCellViewModel {
    
    private var facility: Facility
    
    public init(facility: Facility) {
        self.facility = facility
    }
    
    public func getType() -> String {
        return self.facility.type
    }
   
    
    public func getState() -> String {
        return self.facility.state
    }
    
    public func getDescription () -> String {
        return self.facility.description
    }
    
    public func getGeoCoordX() -> Double {
        return self.facility.geocoordX
    }

    public func getGeoCoordY() -> Double {
        return self.facility.geocoordY
    }

    public func getStateExplanation() -> String {
        return self.facility.stateExplanation
    }
    
    public func getEquipmentnumber() -> Int {
        return self.facility.equipmentnumber
    }
    
    public func getStationnumber() -> Int {
        return self.facility.stationnumber
    }
    

}

