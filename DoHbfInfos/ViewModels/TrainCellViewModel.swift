//
//  TrainCellViewModel.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 29.09.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import Foundation


public class TrainCellViewModel {
    
    private var train: Train
    
    public init(train: Train) {
        self.train = train
    }
    
   
    
    public func getName() -> String {
        return self.train.name
    }
    
    public func getDateTime() -> String {
        return self.train.dateTime
    }
    
    public func getBoardId() -> Int {
        return self.train.boardId
    }
    
    public func getOrigin() -> String {
        return self.train.origin
    }
    
    public func getTrack() -> String {
        return self.train.track
    }
    
    public func getType() -> String {
        return self.train.type
    }
    
    public func getStopId() -> Int {
        return self.train.stopId
    }
    
    public func getDetailsId() -> String {
        return self.train.detailsId
    }
}
