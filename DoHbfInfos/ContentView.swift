//
//  ContentView.swift
//  DoHbfInfos
//
//  Created by Elena Geller on 27.09.19.
//  Copyright © 2019 Elena Geller. All rights reserved.
//

import SwiftUI

import Combine


struct ContentView: View {
   // @State private var selection = 0
   // @State private var richtung = 0
    @ObservedObject var viewModel: TrainListViewModel
    @ObservedObject var stationViewModel: StationViewModel
    @ObservedObject var departuresViewModel: DeparturesViewModel
 
    var body: some View {
        RootView(viewModel: viewModel, stationViewModel: stationViewModel, departuresViewModel: departuresViewModel)
       
    }
}
#if DEBUG

let viewModel = TrainListViewModel();
let stationViewModel = StationViewModel();
let departuresViewModel = DeparturesViewModel(stationId: "8000080");
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: viewModel, stationViewModel: stationViewModel, departuresViewModel: departuresViewModel)
    }
}
#endif
